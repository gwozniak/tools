const gulp = require('gulp')
const rename = require('gulp-rename')
const sass = require('gulp-sass')

const styleSRC = './scss/root.scss'
const styleDIST = './public/styles'

gulp.task('styles_build', function () {
  gulp.src(styleSRC)
    .pipe(sass({
      errorLogToConsole: true,
      outputStyle: 'compressed'
    }))
    .on('error', console.error.bind(console))
    .pipe(rename({
      suffix: '.min',
      basename: 'main'
    }))
    .pipe(gulp.dest(styleDIST))
})

gulp.task('styles_watch', function () {
  gulp.watch('./scss/**/*.scss', ['styles_build'])
})