# Tools

## nodejs-express-starter

It's a NodeJS/Express/ES6 starter that has already implemented:
1) basic dynamic router
2) basic dynamic templating
3) basic Gitlab CI with deployment to Heroku
4) eslint